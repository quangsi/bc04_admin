import React from "react";
import { DesktopView, MobileView, TabletView } from "../../HOC/Reposive";
import HeaderDesktop from "./HeaderDesktop";
import HeaderMobie from "./HeaderMobie";
import HeaderTablet from "./HeaderTablet";

export default function Header() {
  return (
    <div>
      <DesktopView>
        <HeaderDesktop />
      </DesktopView>

      <TabletView>
        <HeaderTablet />
      </TabletView>

      <MobileView>
        <HeaderMobie />
      </MobileView>
    </div>
  );
}
